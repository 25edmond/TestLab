package services;

import models.dao.StudentDAO;
import models.dao.factory.FactoryDAO;
import models.entity.Student;

import java.util.List;

/**
 * Created by developer on 1/19/17.
 */
public class StudentService {
    /**
     * Find an student by code
     *
     * @param  code String
     *
     * @return Student
     */
    public static List<Student> findByClass(String code) {
        return FactoryDAO.getStudentDAO().findByClassroom(code);
    }
    /**
     * Find an student by code
     *
     * @param  code String
     *
     * @return Student
     */
    public static Student find(String code) {
        return FactoryDAO.getStudentDAO().find(code);
    }
    /**
     * Create an student
     *
     * @param data Student
     *
     * @return Student
     */
    public static Student create(Student data) {
        return FactoryDAO.getStudentDAO().create(data);

    }

    /**
     * Update an student
     *
     * @param  data Student
     *
     * @return Student
     */
    public static Student update(Student data) {
        return FactoryDAO.getStudentDAO().update(data);
    }



    /**
     * Delete an student by code
     *
     * @param code String
     */
    public static Boolean delete(String code)  {
        Student student = FactoryDAO.getStudentDAO().find(code);
        if (student != null) {
            FactoryDAO.getStudentDAO().delete(student);
            return true;
        } else {
            return false;
        }
    }

    /**
     * Get all students
     *
     * @return List<Student>
     */
    public static List<Student> all() {
        return FactoryDAO.getStudentDAO().find();
    }
}

