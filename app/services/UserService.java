package services;

import models.dao.DAO;
import models.dao.factory.FactoryDAO;
import models.entity.User;

import java.util.List;

/**
 * Created by developer on 1/13/17.
 */
public class UserService {
    /**
     * Find an employee by id
     *
     * @param  code String
     *
     * @return User
     */
    public static User find(String code)throws Exception {
        return FactoryDAO.getUserDAO().find(code);
    }
    /**
     * Create an employee
     *
     * @param data User
     *
     * @return User
     */
    public static User create(User data) {
        return FactoryDAO.getUserDAO().create(data);

    }

    /**
     * Update an employee
     *
     * @param  data User
     *
     * @return User
     */
    public static User update(User data){
        return FactoryDAO.getUserDAO().update(data);
    }



    /**
     * Delete an employee by id
     *
     * @param code String
     */
    public static Boolean delete(String code) throws Exception {
        DAO<User> dao = FactoryDAO.getUserDAO();
        User user = dao.find(code);
        if (user != null) {
            dao.delete(user);
            return true;
        } else {
            return false;
        }
    }

    /**
     * Get all employees
     *
     * @return List<User>
     */
    public static List<User> all()throws Exception {
        return FactoryDAO.getUserDAO().find();
    }
}
