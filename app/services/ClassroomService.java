package services;

import models.dao.factory.FactoryDAO;
import models.entity.Classroom;
import models.entity.Student;
import models.dao.StudentDAO;
import java.util.List;

/**
 * Created by developer on 1/18/17.
 */
public class ClassroomService {
    /**
     * Find an classroom by id
     *
     * @param  code String
     *
     * @return Classroom
     */
    public static Classroom find(String code)throws Exception {
        return FactoryDAO.getClassroomDAO().find(code);
    }
    /**
     * Create an classroom
     *
     * @param data Classroom
     *
     * @return Classroom
     */
    public static Classroom create(Classroom data)throws Exception {
        return FactoryDAO.getClassroomDAO().create(data);

    }

    /**
     * Update an classroom
     *
     * @param  data Classroom
     *
     * @return Classroom
     */
    public static Classroom update(Classroom data)throws Exception {
        return FactoryDAO.getClassroomDAO().update(data);
    }



    /**
     * Delete an classroom by id
     *
     * @param code String
     */
    public static Boolean delete(String code) throws Exception {
        Classroom classroom = FactoryDAO.getClassroomDAO().find(code);
        if (classroom != null) {
            StudentDAO stDao = FactoryDAO.getStudentDAO();
            List<Student> sts= stDao.findByClassroom(code);
            for (Student st: sts) {
                stDao.delete(st);
            }
            FactoryDAO.getClassroomDAO().delete(classroom);
            return true;
        } else {
            return false;
        }
    }

    /**
     * Get all classrooms
     *
     * @return List<Classroom>
     */
    public static List<Classroom> all()throws Exception {
        return FactoryDAO.getClassroomDAO().find();
    }
}
