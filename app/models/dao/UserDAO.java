package models.dao;

import models.entity.User;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by developer on 1/24/17.
 */
public class UserDAO extends DAO<User> {

    @Override
    public List<User> find() {
        List<User> users = new ArrayList<User>();
        User user;
        try {
            ResultSet result = this.connect
                    .createStatement(
                            ResultSet.TYPE_SCROLL_INSENSITIVE,
                            ResultSet.CONCUR_UPDATABLE
                    ).executeQuery(
                            "SELECT * FROM user "
                    );
            while (result.next()) {
                user = new User(
                        result.getString("code"),
                        result.getString("login"),
                        result.getString("password"),
                        result.getDate("creat_at"),
                        result.getDate("update_at")
                );
                users.add(user);
            }
        } catch (SQLException e) {
            logger.error(e.getMessage());
        }finally {
            try {
                connect.close();
            } catch (SQLException e) {
                e.printStackTrace();
                logger.error(e.getMessage());
            }
        }
        logger.info("liste des classe generer avec succes");
        return users;
    }

    @Override
    public User find(String code) {
        User user = null;
        try {

            ResultSet result = this.connect
                    .createStatement(
                            ResultSet.TYPE_SCROLL_INSENSITIVE,
                            ResultSet.CONCUR_UPDATABLE
                    ).executeQuery(
                            "SELECT * FROM user WHERE code = '" + code + "'"
                    );
            ResultSet resul = null;
            while (result.next()) {
                user = new User(
                        result.getString("code"),
                        result.getString("login"),
                        result.getString("password"),
                        result.getDate("creat_at"),
                        result.getDate("update_at")
                );

            }
        } catch (SQLException e) {
            logger.error(e.getMessage());
        }finally {
            try {
                connect.close();
            } catch (SQLException e) {
                e.printStackTrace();
                logger.error(e.getMessage());
            }
        }
        logger.info(" une ligne de user a ete obtenue avec succes");
        return user;
    }

    @Override
    public User create(User user) {
        try {

            PreparedStatement prepare = this.connect
                    .prepareStatement(
                            "INSERT INTO user (code,login,password,creat_at,update_at) VALUES(?, ?, ?, ?, ?)"
                    );
            prepare.setString(1, user.getCode());
            prepare.setString(2, user.getLogin());
            prepare.setString(3, user.getPassword());
            prepare.setDate(4, user.getCreateAt());
            prepare.setDate(5, user.getUpdateAt());
            prepare.executeUpdate();
            user = this.find(user.getCode());

        } catch (SQLException e) {
            logger.error(e.getMessage());
        }finally {
            try {
                connect.close();
            } catch (SQLException e) {
                e.printStackTrace();
                logger.error(e.getMessage());
            }
        }
        logger.info(" une ligne de user a ete creer avec succes");
        return user;
    }

    @Override
    public User update(User user) {
        int i = 0;
        try {

            PreparedStatement prepare = this.connect
                    .prepareStatement(
                            "UPDATE user SET login=? , password=? , creat_at=?, update_at=? WHERE code = ? "
                    );
            prepare.setString(1, user.getLogin());
            prepare.setString(2, user.getPassword());
            prepare.setDate(3, user.getCreateAt());
            prepare.setDate(4, user.getUpdateAt());
            prepare.setString(5, user.getCode());
            i = prepare.executeUpdate();
            user = this.find(user.getCode());
        } catch (SQLException e) {
            logger.error(e.getMessage());
        }finally {
            try {
                connect.close();
            } catch (SQLException e) {
                e.printStackTrace();
                logger.error(e.getMessage());
            }
        }
        logger.info(i + " ligne de user a ete mis a jour avec succes");
        return user;
    }

    @Override
    public boolean delete(User user) {
        int i = 0;
        try {

            i = this.connect
                    .createStatement(
                            ResultSet.TYPE_SCROLL_INSENSITIVE,
                            ResultSet.CONCUR_UPDATABLE
                    ).executeUpdate(
                            "DELETE FROM user WHERE code = '" + user.getCode() + "'"
                    );

        } catch (SQLException e) {
            logger.error(e.getMessage());
        }finally {
            try {
                connect.close();
            } catch (SQLException e) {
                e.printStackTrace();
                logger.error(e.getMessage());
            }
        }
        logger.info(i + " ligne de user a ete supprime");
        return (i != 0);
    }
}
