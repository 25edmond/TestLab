package models.dao;

import play.Logger;
import play.db.DB;

import java.sql.Connection;
import java.util.List;

/**
 * Created by developer on 1/24/17.
 */
public abstract class DAO<T>  {

    public static Connection connect = DB.getConnection();
    protected static final Logger.ALogger logger = Logger.of(DAO.class);
    /**
     * Permet de récupérer des objets
     * @return
     */
    public  abstract List<T> find();
    /**
     * Permet de récupérer un objet grace a code
     * @param code
     * @return
     */
    public  abstract T find(String code);

    /**
     * Permet de créer une entrée dans la base de données
     * par rapport à un objet
     * @param obj
     */
    public abstract T create(T obj);

    /**
     * Permet de mettre à jour les données d'une entrée dans la base
     * @param obj
     */
    public abstract T update(T obj);

    /**
     * Permet la suppression d'une entrée de la base
     * @param obj
     */
    public abstract boolean delete(T obj);
}
