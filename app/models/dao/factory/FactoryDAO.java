package models.dao.factory;

import models.dao.ClassroomDAO;
import models.dao.DAO;
import models.dao.StudentDAO;
import models.dao.UserDAO;
import models.entity.Classroom;
import models.entity.Student;
import models.entity.User;

/**
 * Created by developer on 1/24/17.
 */
public class  FactoryDAO {
    public static ClassroomDAO getClassroomDAO(){
        return new ClassroomDAO();
    }
    public static StudentDAO getStudentDAO(){return new StudentDAO();}
    public static UserDAO getUserDAO(){return new UserDAO();}
}
