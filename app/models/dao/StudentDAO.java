package models.dao;

import models.entity.Classroom;
import models.entity.Student;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by developer on 1/24/17.
 */
public class StudentDAO extends DAO<Student> {
    @Override
    public List<Student> find() {
        List<Student> students = new ArrayList<Student>();
        Student student = null;
        Classroom classroom = null;
        try {
            ResultSet result = this.connect
                    .createStatement(
                            ResultSet.TYPE_SCROLL_INSENSITIVE,
                            ResultSet.CONCUR_UPDATABLE
                    ).executeQuery(
                            "SELECT * FROM student "
                    );
            ResultSet resul = null;
            while (result.next()) {
                student = new Student(
                        result.getString("code"),
                        result.getString("name"),
                        result.getString("surname"),
                        result.getDate("born_at")
                );
                resul = this.connect
                        .createStatement(
                                ResultSet.TYPE_SCROLL_INSENSITIVE,
                                ResultSet.CONCUR_UPDATABLE
                        ).executeQuery(
                                "SELECT * FROM classroom WHERE code ='" + result.getString("classroom") + "'"
                        );
                if (resul.next()) {
                    classroom = new Classroom(
                            resul.getString("code"),
                            resul.getString("name"),
                            resul.getInt("level"),
                            resul.getString("ecole")
                    );
                }
                student.setLevel(classroom);
                students.add(student);
            }
        } catch (SQLException e) {
            logger.error(e.getMessage());
        }
        logger.info("liste des eleves generer avec succes");
        return students;
    }

    @Override
    public Student create(Student student) {
        try {

            PreparedStatement prepare = this.connect
                    .prepareStatement(
                            "INSERT INTO student (code,name,surname,born_at,classroom) VALUES(?, ?, ?, ?, ?)"
                    );
            prepare.setString(1, student.getCode());
            prepare.setString(2, student.getName());
            prepare.setString(3, student.getSurname());
            prepare.setDate(4, student.getBornAt());
            prepare.setString(5, student.getLevel().getCode());
            prepare.executeUpdate();
            student = this.find(student.getCode());

        } catch (SQLException e) {
            logger.error(e.getMessage());
        }
        logger.info(" une ligne de student a ete creer avec succes");
        return student;
    }


    @Override
    public Student find(String code) {
        Student student = null;
        Classroom classroom = null;
        try {

            ResultSet result = this.connect
                    .createStatement(
                            ResultSet.TYPE_SCROLL_INSENSITIVE,
                            ResultSet.CONCUR_UPDATABLE
                    ).executeQuery(
                            "SELECT * FROM student WHERE code = '" + code + "'"
                    );
            ResultSet resul = null;
            while (result.next()) {
                student = new Student(
                        result.getString("code"),
                        result.getString("name"),
                        result.getString("surname"),
                        result.getDate("born_at")
                );
                resul = this.connect
                        .createStatement(
                                ResultSet.TYPE_SCROLL_INSENSITIVE,
                                ResultSet.CONCUR_UPDATABLE
                        ).executeQuery(
                                "SELECT * FROM classroom WHERE code ='" + result.getString("classroom") + "'"
                        );
                if (resul.next()) {
                    classroom = new Classroom(
                            resul.getString("code"),
                            resul.getString("name"),
                            resul.getInt("level"),
                            resul.getString("ecole")
                    );
                }
                student.setLevel(classroom);
            }
        } catch (SQLException e) {
            logger.error(e.getMessage());
        }
        logger.info(" une ligne de student a ete obtenue avec succes");
        return student;

    }

    public List<Student> findByClassroom(String codeClass) {
        List<Student> students = new ArrayList<Student>();
        Student student = null;
        Classroom classroom = null;
        try {
            ResultSet resul = this.connect
                    .createStatement(
                            ResultSet.TYPE_SCROLL_INSENSITIVE,
                            ResultSet.CONCUR_UPDATABLE
                    ).executeQuery(
                            "SELECT * FROM classroom WHERE code ='" + codeClass + "'"
                    );
            if (resul.next()) {
                classroom = new Classroom(
                        resul.getString("code"),
                        resul.getString("name"),
                        resul.getInt("level"),
                        resul.getString("ecole")
                );
            }
            ResultSet result = this.connect
                    .createStatement(
                            ResultSet.TYPE_SCROLL_INSENSITIVE,
                            ResultSet.CONCUR_UPDATABLE
                    ).executeQuery(
                            "SELECT * FROM student WHERE classroom ='"+codeClass+"'"
                    );


            while (result.next()) {
                student = new Student(
                        result.getString("code"),
                        result.getString("name"),
                        result.getString("surname"),
                        result.getDate("born_at")
                );
                student.setLevel(classroom);
                students.add(student);
            }


        } catch (SQLException e) {
            logger.error(e.getMessage());
        }
        logger.info("liste des eleves de "+classroom.getName()+" generer avec succes");
        return students;
    }


    @Override
    public Student update(Student student) {
        int i = 0;
        try {

            PreparedStatement prepare = this.connect
                    .prepareStatement(
                            "UPDATE student SET name=? , surname=? , born_at=?, classroom=? WHERE code = ? "
                    );
            prepare.setString(1, student.getName());
            prepare.setString(2, student.getSurname());
            prepare.setDate(3, student.getBornAt());
            prepare.setString(4, student.getLevel().getCode());
            prepare.setString(5, student.getCode());
            i = prepare.executeUpdate();
            student = this.find(student.getCode());
        } catch (SQLException e) {
            logger.error(e.getMessage());
        }
        logger.info(i + " ligne de student a ete mis a jour avec succes");
        return student;
    }


    @Override
    public boolean delete(Student student) {
        int i = 0;
        try {

            i = this.connect
                    .createStatement(
                            ResultSet.TYPE_SCROLL_INSENSITIVE,
                            ResultSet.CONCUR_UPDATABLE
                    ).executeUpdate(
                            "DELETE FROM student WHERE code = '" + student.getCode() + "'"
                    );

        } catch (SQLException e) {
            logger.error(e.getMessage());
        }
        logger.info(i + " ligne de student a ete supprime");
        return (i != 0);
    }
}
