package models.dao;

import models.entity.Classroom;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by developer on 1/24/17.
 */
public class ClassroomDAO extends DAO<Classroom> {
    @Override
    public List<Classroom> find() {
        List<Classroom> classrooms = new ArrayList<Classroom>();
        Classroom classroom;
        try {
            ResultSet result = this.connect
                    .createStatement(
                            ResultSet.TYPE_SCROLL_INSENSITIVE,
                            ResultSet.CONCUR_UPDATABLE
                    ).executeQuery(
                            "SELECT * FROM classroom "
                    );
            while (result.next()) {
                classroom = new Classroom(
                        result.getString("code"),
                        result.getString("name"),
                        result.getInt("level"),
                        result.getString("ecole")
                );
                classrooms.add(classroom);
            }
        } catch (SQLException e) {
            logger.error(e.getMessage());
        }finally {
            try {
                connect.close();
            } catch (SQLException e) {
                e.printStackTrace();
                logger.error(e.getMessage());
            }
        }
        logger.info("liste des classe generer avec succes");
        return classrooms;
    }

    @Override
    public Classroom create(Classroom classr) {
        try {

            PreparedStatement prepare = this.connect
                    .prepareStatement(
                            "INSERT INTO classroom (code,name,level,ecole) VALUES(?, ?, ?, ?)"
                    );
            prepare.setString(1, classr.getCode());
            prepare.setString(2, classr.getName());
            prepare.setInt(3, classr.getLevel());
            prepare.setString(4, classr.getEcole());
            prepare.executeUpdate();
            classr = this.find(classr.getCode());

        } catch (SQLException e) {
            logger.error(e.getMessage());
        }finally {
            try {
                connect.close();
            } catch (SQLException e) {
                e.printStackTrace();
                logger.error(e.getMessage());
            }
        }
        logger.info(" une ligne de classroom a ete creer avec succes");
        return classr;
    }


    @Override
    public  Classroom find(String code) {
        Classroom classroom = new Classroom();
        try {
            ResultSet result = this.connect
                    .createStatement(
                            ResultSet.TYPE_SCROLL_INSENSITIVE,
                            ResultSet.CONCUR_UPDATABLE
                    ).executeQuery(
                            "SELECT * FROM classroom WHERE code = '" + code + "'"
                    );
            if (result.next())
                classroom = new Classroom(
                        code,
                        result.getString("name"),
                        result.getInt("level"),
                        result.getString("ecole")
                );

        } catch (SQLException e) {
            logger.error(e.getMessage());
        }finally {
            try {
                connect.close();
            } catch (SQLException e) {
                e.printStackTrace();
                logger.error(e.getMessage());
            }
        }
        logger.info(" une ligne de classroom a ete obtenue avec succes");
        return classroom;

    }


    @Override
    public  Classroom update(Classroom classr) {
        int i=0;
        try {

            PreparedStatement prepare = this.connect
                    .prepareStatement(
                            "UPDATE classroom SET name=? , level=? , ecole=? WHERE code = ? "
                    );
            prepare.setString(1, classr.getName());
            prepare.setInt(2, classr.getLevel());
            prepare.setString(3, classr.getEcole());
            prepare.setString(4, classr.getCode());
            i = prepare.executeUpdate();
            classr = this.find(classr.getCode());
        } catch (SQLException e) {
            logger.error(e.getMessage());
        }finally {
            try {
                connect.close();
            } catch (SQLException e) {
                e.printStackTrace();
                logger.error(e.getMessage());
            }
        }
        logger.info(i+" ligne de classroom a ete mis a jour avec succes");
        return classr;
    }


    @Override
    public  boolean delete(Classroom classr) {
        int i=0;
        try {

            i = this.connect
                    .createStatement(
                            ResultSet.TYPE_SCROLL_INSENSITIVE,
                            ResultSet.CONCUR_UPDATABLE
                    ).executeUpdate(
                            "DELETE FROM classroom WHERE code = '" + classr.getCode() + "'"
                    );

        } catch (SQLException e) {
            logger.error(e.getMessage());
        }finally {
            try {
                connect.close();
            } catch (SQLException e) {
                e.printStackTrace();
                logger.error(e.getMessage());
            }
        }
        logger.info(i+" ligne de classroom a ete supprime");
        return (i!=0);
    }
}