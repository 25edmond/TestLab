package models.entity;

/**
 * Created by developer on 1/24/17.
 */
public class Classroom {

    private String code;
    private String name;
    private int level;
    private String ecole;

    public Classroom(String code, String name, int level, String ecole) {
        this.code = code;
        this.name = name;
        this.level = level;
        this.ecole = ecole;
    }

    public Classroom() {
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public int getLevel() {
        return level;
    }

    public String getEcole() {
        return ecole;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public void setEcole(String ecole) {
        this.ecole = ecole;
    }
}
