package models.entity;

import java.sql.Date;

/**
 * Created by developer on 1/24/17.
 */
public class Student {

    private String code;
    private String name;
    private String surname;
    private Date bornAt;
    private Classroom level;

    public Student(String code, String name, String surname, Date bornAt) {
        this.code = code;
        this.name = name;
        this.surname = surname;
        this.bornAt = bornAt;
    }

    public Student() {

    }

    public void setLevel(Classroom level) {
        this.level = level;
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public Date getBornAt() {
        return bornAt;
    }

    public Classroom getLevel() {
        return level;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setBornAt(Date bornAt) {
        this.bornAt = bornAt;
    }
}
