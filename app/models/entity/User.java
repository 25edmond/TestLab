package models.entity;

import java.sql.Date;

import play.data.format.Formats;
import play.data.validation.Constraints;

/**
 * Created by developer on 1/24/17.
 */
public class User {
    @Constraints.Required
    //@Formats.NonEmpty
    public String code;
    @Constraints.Required
    public  String login;
    @Constraints.Required
    public  String password;
    @Constraints.Required
    @Formats.DateTime(pattern = "yyyy-MM-dd")
    public  Date createAt;
    @Constraints.Required
    @Formats.DateTime(pattern = "yyyy-MM-dd")
    public  Date updateAt;

    public User(String login, String password) {
        this.login = login;
        this.password = password;
    }

    public User(String code, String login, String password, Date creatAt, Date updateAt) {
        this.code = code;
        this.login = login;
        this.password = password;
        this.createAt = creatAt;
        this.updateAt = updateAt;
    }

    public User() {

    }

    public String getCode() {
        return code;
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }

    public Date getCreateAt() {
        return createAt;
    }

    public Date getUpdateAt() {
        return updateAt;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setCreateAt(Date createAt) {
        this.createAt = createAt;
    }

    public void setUpdateAt(Date updateAt) {
        this.updateAt = updateAt;
    }
}
