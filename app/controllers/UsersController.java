package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import models.entity.Users;
import play.data.DynamicForm;
import play.data.FormFactory;
import play.libs.Json;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by developer on 1/25/17.
 */
public class UsersController extends Controller {

    Form<Users> userForm = Form.form(Users.class);

    public Result create(){
       // JsonNode json = request().body().asJson();
/*        Map<String,String> data = new HashMap<>();
        data.put("email", json.findPath("email").textValue());
        data.put("password", json.findPath("password").textValue());*/
      /*  DynamicForm requestData = Form.form().bindFromRequest();
        String firstname = requestData.get("firstname");
        String lastname = requestData.get("lastname");
        return ok("Hello " + firstname + " " + lastname);*/

        Form<Users> usersForm = userForm.bindFromRequest();
        if (usersForm.hasErrors()) {
            return badRequest(usersForm.errorsAsJson());
        }
        return ok(Json.toJson(usersForm.get())).as("application/json");
    }
}
