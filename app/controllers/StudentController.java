package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import models.entity.Student;
import play.libs.Json;
import play.mvc.BodyParser;
import play.mvc.Controller;
import play.mvc.Result;
import services.ClassroomService;
import services.StudentService;

import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.UUID;

/**
 * Created by developer on 1/24/17.
 */
public class StudentController extends Controller {
    public static Result list() {
        return play.mvc.Results.TODO;
    }

    public static Result listByClass(String code) {
        return play.mvc.Results.TODO;
    }

    public static Result get(String code) {
        return play.mvc.Results.TODO;
    }

    public static Result create() {
        return play.mvc.Results.TODO;
    }

    public static Result update() {
        return play.mvc.Results.TODO;
    }

    public static Result delete(String code) {
        return play.mvc.Results.TODO;
    }

   /* @BodyParser.Of(BodyParser.Json.class)
    public static Result list() {
        JsonNode studentsJson = null;
        try {
            studentsJson = Json.toJson(StudentService.all());
            //msg.put("list", );
        } catch (Exception e) {
            e.printStackTrace();
            return internalServerError();
        }
        return ok(studentsJson);
    }
    ###
    #classroom
GET           /classroom                  controllers.ClassroomController.list()
GET           /classroom/:code            controllers.ClassroomController.get(code:String)
POST          /classroom                  controllers.ClassroomController.create()
PUT           /classroom                  controllers.ClassroomController.update()
PATCH         /classroom                  controllers.ClassroomController.update()
DELETE        /classroom/:code            controllers.ClassroomController.delete(code:String)

#student
GET           /student                    controllers.StudentController.list()
GET           /student/class/:code        controllers.StudentController.listByClass(code:String)
GET           /student/:code              controllers.StudentController.get(code:String)
POST          /student                    controllers.StudentController.create()
PUT           /student                    controllers.StudentController.update()
PATCH         /student                    controllers.StudentController.update()
DELETE        /student/:code              controllers.StudentController.delete(code:String)
    ###

    @BodyParser.Of(BodyParser.Json.class)
    public static Result listByClass(String code) {
        JsonNode studentsJson = null;
        try {
            System.out.println(StudentService.findByClass(code));
            studentsJson = Json.toJson(StudentService.findByClass(code));
            //msg.put("list", );
        } catch (Exception e) {
            e.printStackTrace();
            return internalServerError();
        }
        return ok(studentsJson);
    }
    @BodyParser.Of(BodyParser.Json.class)
    public static Result get(String code) {
        JsonNode studentJson = null;
        try {
            studentJson = Json.toJson(StudentService.find(code));
        } catch (Exception e) {
            e.printStackTrace();
            return internalServerError();
        }
        return ok(studentJson);
    }

    @BodyParser.Of(BodyParser.Json.class)
    public static Result create() {
        JsonNode studentJson = null;
        Student student=null;
        try {
            // get parameters of request as object json
            JsonNode json = request().body().asJson();
            // construct student object with parameters where you are
            student = new Student();
            student.setCode(UUID.randomUUID().toString());
            student.setName(json.findPath("name").textValue());
            student.setSurname(json.findPath("surname").textValue());
            // format date (String -> Date)
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            String dateInString = json.findPath("born_at").textValue();
            Date dateSQL=null;
            try {
                java.util.Date date = formatter.parse(dateInString);
                dateSQL = new java.sql.Date(date.getTime());
            } catch (ParseException e) {
                e.printStackTrace();
            }
            student.setBornAt(dateSQL);
            student.setLevel(ClassroomService.find(json.findPath("classroom").textValue()));
            //create on BD
            studentJson = Json.toJson(StudentService.create(student));
        } catch (Exception e) {
            e.printStackTrace();
            return internalServerError(e.getMessage());
        }
        //System.out.println(u);
        return ok(studentJson);
    }

    @BodyParser.Of(BodyParser.Json.class)
    public static Result update() {
        // get parameters of request as object json
        Student student = null;
        JsonNode studentJson = null;
        try {
            JsonNode json = request().body().asJson();
            student = new Student();
            student.setCode(json.findPath("code").textValue());
            student.setName(json.findPath("name").textValue());
            student.setSurname(json.findPath("surname").textValue());
            // format date (String -> Date)
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            String dateInString = json.findPath("born_at").textValue();
            Date dateSQL=null;
            try {
                java.util.Date date = formatter.parse(dateInString);
                dateSQL = new java.sql.Date(date.getTime());
            } catch (ParseException e) {
                e.printStackTrace();
            }
            student.setBornAt(dateSQL);
            student.setLevel(ClassroomService.find(json.findPath("classroom").textValue()));
            //create on BD
            studentJson = Json.toJson(StudentService.update(student));

        } catch (Exception e) {
            e.printStackTrace();
            return internalServerError();
        }
        return ok(studentJson);
    }
    @BodyParser.Of(BodyParser.Json.class)
    public static Result delete(String code) {
        JsonNode studentJson = null;
        try {
            studentJson = Json.toJson(StudentService.delete(code));
        } catch (Exception e) {
            e.printStackTrace();
            return internalServerError();
        }
        return ok(studentJson);
    }*/
}
