package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import models.entity.Classroom;
import play.libs.Json;
import play.mvc.BodyParser;
import play.mvc.Controller;
import play.mvc.Result;
import services.ClassroomService;

import java.util.UUID;

import static play.mvc.Http.Context.Implicit.request;

/**
 * Created by developer on 1/24/17.
 */
public class ClassroomController extends Controller {
    @BodyParser.Of(BodyParser.Json.class)
    public static Result list() {
        JsonNode classroomsJson = null;
        try {
            classroomsJson = Json.toJson(ClassroomService.all());
            //msg.put("list", );
        } catch (Exception e) {
            e.printStackTrace();
            return internalServerError();
        }
        return ok(classroomsJson);
    }
    @BodyParser.Of(BodyParser.Json.class)
    public static Result get(String code) {
        JsonNode classroomJson = null;
        try {
            classroomJson = Json.toJson(ClassroomService.find(code));
        } catch (Exception e) {
            e.printStackTrace();
            return internalServerError();
        }
        return ok(classroomJson);
    }

    @BodyParser.Of(BodyParser.Json.class)
    public static Result create() {
        JsonNode classroomJson = null;
        Classroom classroom=null;
        try {
            // get parameters of request as object json
            JsonNode json = request().body().asJson();
            // construct classroom object with parameters where you are
            classroom = new Classroom();
            classroom.setCode(UUID.randomUUID().toString());
            classroom.setName(json.findPath("name").textValue());
            classroom.setLevel(json.findPath("level").asInt());
            classroom.setEcole(json.findPath("ecole").textValue());
            //create on BD
            classroomJson = Json.toJson(ClassroomService.create(classroom));
        } catch (Exception e) {
            e.printStackTrace();
            return internalServerError(e.getMessage());
        }
        //System.out.println(u);
        return ok(classroomJson);
    }

    @BodyParser.Of(BodyParser.Json.class)
    public static Result update() {
        // get parameters of request as object json
        Classroom classroom = null;
        JsonNode classroomJson = null;
        try {
            JsonNode json = request().body().asJson();
            classroom = new Classroom();
            classroom.setCode(json.findPath("code").textValue());
            classroom.setName(json.findPath("name").textValue());
            classroom.setLevel(json.findPath("level").asInt());
            classroom.setEcole(json.findPath("ecole").textValue());
            //create on BD
            classroomJson = Json.toJson(ClassroomService.update(classroom));

        } catch (Exception e) {
            e.printStackTrace();
            return internalServerError();
        }
        return ok(classroomJson);
    }
    @BodyParser.Of(BodyParser.Json.class)
    public static Result delete(String code) {
        //session("user","25edmond");
        JsonNode classroomJson = null;
        try {
            classroomJson = Json.toJson(ClassroomService.delete(code));
        } catch (Exception e) {
            e.printStackTrace();
            return internalServerError();
        }
        return ok(classroomJson);
    }
}
