package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import models.entity.User;
import play.Logger;
import play.data.Form;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import services.UserService;

import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * Created by developer on 1/24/17.
 */
public class UserController extends Controller {

    private static final Logger.ALogger logger = Logger.of(UserController.class);

    private Form<User> userForm = Form.form(User.class);

    public Result list() {
        JsonNode usersJson = null;
        try {
            usersJson = Json.toJson(UserService.all());
            //
        } catch (Exception e) {
            e.printStackTrace();
            return internalServerError();
        }
        return ok(usersJson).as("application/json");
    }

    public Result get(String code) {
        JsonNode userJson = null;
        try {
            userJson = Json.toJson(UserService.find(code));
        } catch (Exception e) {
            e.printStackTrace();
            return internalServerError();
        }
        return ok(userJson).as("application/json");
    }

    public Result create() {
        JsonNode userJson = null;
        try {
            // get parameters of request as object json
            JsonNode json = request().body().asJson();
            //
            Map<String,String> userData = new HashMap<>();
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            String dateInString = json.findPath("create_at").textValue();
            Date dateSQL=null;
            try {
                java.util.Date date = formatter.parse(dateInString);
                dateSQL = new java.sql.Date(date.getTime());
            } catch (ParseException e) {
                e.printStackTrace();
                badRequest("Format date create_at is 'yyyy-MM-dd'");
            }
            // construct user object with parameters where you are
            userData.put("code",UUID.randomUUID().toString());
            userData.put("login",json.findPath("login").textValue());
            userData.put("password",json.findPath("password").textValue());
            userData.put("createAt",dateInString);
            userData.put("updateAt",dateInString);
            logger.info("Login : "+json.findPath("login").textValue()+" || Password : "+json.findPath("password").textValue());

            //create on BD
            Form<User> u = userForm.bind(userData);
            if (u.hasErrors()) {
                return badRequest(u.errorsAsJson());
            }
            User user = u.get();
            userJson = Json.toJson(UserService.create(user));

        } catch (Exception e) {
            e.printStackTrace();
            return internalServerError(e.getMessage());
        }
        //
        return ok(userJson).as("application/json");
    }

    public Result update() {
        return ok("update one student");
    }

    public Result delete(String code) {
        return ok("delete one student");
    }


/*
Map<String,String> anyData = new HashMap();
anyData.put("email", "bob@gmail.com");
anyData.put("password", "secret");

User user = userForm.bind(anyData).get();


        Form<Employee> employee = employeeForm.bindFromRequest();
        if (employee.hasErrors()) {
            return jsonResult(badRequest(employee.errorsAsJson()));
        }
        Employee newEmployee = EmployeeService.create(employee.get());
        return jsonResult(created(Json.toJson(newEmployee)));

   
    public static Result create() {
        JsonNode userJson = null;
        User u;
        try {
            // get parameters of request as object json
            JsonNode json = request().body().asJson();
            //
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            String dateInString = json.findPath("create_at").textValue();
            Date dateSQL=null;
            try {
                java.util.Date date = formatter.parse(dateInString);
                dateSQL = new java.sql.Date(date.getTime());
            } catch (ParseException e) {
                e.printStackTrace();
            }
            // construct user object with parameters where you are
            User user = new User();
            user.setCode(UUID.randomUUID().toString());
            logger.info("Login : "+json.findPath("login").textValue()+" || Password : "+json.findPath("password").textValue());
            user.setLogin(json.findPath("login").textValue());
            user.setPassword(json.findPath("password").textValue());
            user.setCreateAt(dateSQL);
            user.setUpdateAt(dateSQL);
            //create on BD
            u = UserService.create(user);
            userJson = Json.toJson(user);
            if (user.getLogin() == null || user.getPassword() == null) {
                return badRequest("Missing parameter [login],[password]");
            }
        } catch (Exception e) {
            e.printStackTrace();
            return internalServerError(e.getMessage());
        }
        //
        return ok(userJson);
    }

   
    public static Result update() {
        // get parameters of request as object json
        User user = new User();
        JsonNode userJson = null;
        User u;
        try {
            JsonNode json = request().body().asJson();
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            String dateCr = json.findPath("create_at").textValue();
            String dateUp = json.findPath("update_at").textValue();
            Date dateC=null,dateU=null;
            try {
                java.util.Date dateud = formatter.parse(dateUp);
                java.util.Date dateCe = formatter.parse(dateCr);
                dateC = new java.sql.Date(dateCe.getTime());
                dateU = new java.sql.Date(dateud.getTime());
            } catch (ParseException e) {
                e.printStackTrace();
            }
            user.setCode(json.get("code").asText());
            user.setLogin(json.get("login").asText());
            user.setPassword(json.get("password").asText());
            user.setCreateAt(dateC);
            user.setUpdateAt(dateU);
            u = UserService.update(user);
            userJson = Json.toJson(user);

        } catch (Exception e) {
            e.printStackTrace();
            return internalServerError();
        }
        return ok(userJson);
    }

    public static Result delete(String code) {
        JsonNode userJson = null;
        try {
            userJson = Json.toJson(UserService.delete(code));
        } catch (Exception e) {
            e.printStackTrace();
            return internalServerError();
        }
        return ok(userJson);
    }*/
}
